import org.apache.ignite.Ignite;
import org.apache.ignite.Ignition;

/**
 * */
public class Main {
    public static void main(String[] args) throws InterruptedException {
        Ignition.setClientMode(true);
        try(Ignite ignite = Ignition.start()) {
            while(true) {
                Thread.sleep(1000);
            }
        }
    }
}
